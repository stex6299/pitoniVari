# What's here
Il file [acquisisci.py](./acquisisci.py) crea due finestre:
* Nella prima viene visualizzato il segnale solo se supera la soglia e non si trova troppo appicicato ai bordi (100 di margine). Tutto hard-codato
* Nella seconda viene creato lo spettro in tempo reale, aggiornato ogni  5 secondi

La threshold è codificata nella variabile `thr` ([linea 85](./acquisisci.py#L85))

# Istruzioni di installazione
1. Installare anaconda
2. Cercare su start "anaconda power shell"
3. Installare il modulo `soundcard` scrivendo `pip install soundcard`
4. Installare il modulo `opencv` scrivendo `pip install opencv-python`
5. Recarsi nella cartella con il file [acquisisci.py](./acquisisci.py)
6. Selezionare come dispositivo di input predefinito di sistema quello a cui è attaccato il rivelatore
7. Avviarlo scrivendo `python acquisisci.py` (oppure su un sistema unix `./acquisisci.py`
8. Per terminare premere "q" oppure "Ctrl + C". Inserire un nome file per salvarlo, alternativamente invio ue volte per uscire

to be done..

#OSCI AUDIO -- OLD
Il file [script.py](./script.py) crea due finestre:
* Nella prima viene visualizzato il segnale solo se supera la soglia e non si trova troppo appicicato ai bordi. Tutto hard-codato
* Nella seconda viene creato lo spettro in tempo reale

È stato predisposto che col `Ctrl+C` venga salvato lo spettro, ma deve ancora essere fatto