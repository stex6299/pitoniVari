# -*- coding: utf-8 -*-
"""
Created on Tue Jan 11 11:31:47 2022

@author: steca

Contenuto del gran passo: 
    https://ec.europa.eu/health/sites/default/files/ehealth/docs/covid-certificate_json_specification_en.pdf
"""

# Scheme of JSON response

# Camera
# https://www.youtube.com/watch?v=IOhZqmSrjlE&ab_channel=Pythonenthusiast
import cv2
from pyzbar.pyzbar import decode

# Green
# https://ethicalmente.it/come-decodificare-il-green-pass-con-python/

import json
import sys
import zlib

import base45
import cbor2
from cose.messages import CoseMessage
import time



#%%
cap = cv2.VideoCapture(0)

cap.set(3, 640)
cap.set(4, 480)

camera = True

while camera:
    success, frame = cap.read()
    
    for code in decode(frame):
        print(code.type)
        print(code.data.decode("utf-8"))
        
        payload = code.data.decode("utf-8")[4:]
        decoded = base45.b45decode(payload)
        decompressed = zlib.decompress(decoded)
        cose = CoseMessage.decode(decompressed)
        print(json.dumps(cbor2.loads(cose.payload), indent=2))
        time.sleep(5)
        
    cv2.imshow("Titolo", frame)
    cv2.waitKey(1) # Continuous live video (1ms)
